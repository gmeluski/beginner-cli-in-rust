pub fn find_matches(content: &str, pattern: &str, mut writer: impl std::io::Write) {
    println!("And...");
    for line in content.lines() {
        if line.contains(pattern) {
            writeln!(writer, "{}", line);
        }
    }
}
