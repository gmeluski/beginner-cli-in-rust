#![allow(unused)]

use structopt::StructOpt;
use anyhow::{Context, Result};
use log::{info, warn};

/// Search for a pattern in a file and display the lines that contain it.
#[derive(StructOpt)]
struct Cli {
    /// The pattern to look for
    pattern: String,
    /// The path to the file to read
    #[structopt(parse(from_os_str))]
    path: std::path::PathBuf,
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let args = Cli::from_args();

    env_logger::init();
    hello();
    info!("starting up");
    warn!("oops, now what?");
    let path = "test.txt";
    let content = std::fs::read_to_string(&args.path)
        .with_context(|| format!("could not read file `{}`", path))?;

    println!("file content: {}", content);
    grrs::find_matches(&content, &args.pattern, &mut std::io::stdout());
    Ok(())
}

fn answer() -> u32 {
    return 42;
}

fn hello() {
    let progress_bar = indicatif::ProgressBar::new(100);
    for i in 0..100 {
        progress_bar.println(format!("[+] finished #{}", i));
        progress_bar.inc(1);
    }

    progress_bar.finish_with_message("done");
}

#[test]
fn check_answer_validity() {
    assert_eq!(answer(), 42);
}

#[test]
fn find_a_match() {
    let mut result = Vec::new();
    grrs::find_matches("lorem ipsum\ndolor sit amet", "lorem", &mut result);
    assert_eq!(result, b"lorem ipsum\n");
}
